import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ) {}

  create(createCustomerDto: CreateCustomerDto) {
    return this.customerRepository.save(createCustomerDto);
  }

  findAll() {
    return this.customerRepository.find();
  }

  async findOne(id: number) {
    const customer = await this.customerRepository.findOne({
      where: { id: id },
      relations: ['orders'],
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return customer;
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const customer = await this.customerRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    const updateCustomer = { ...customer, ...updateCustomerDto };
    return this.customerRepository.save(updateCustomer);
  }

  async remove(id: number) {
    const customer = await this.customerRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    return this.customerRepository.softRemove(customer);
  }
}
